# Przykład demonstruje odczyt stanu czujników podłoża

# Z czujników podłoża jest odczytywane napięcie

# import bibliotek
from sumolib import *
import time

print('Przyklad 4')

# czujniki podłoża
grd1, grd2, grd3, grd4 = grds_init()
# diody
led1, led2 = leds_init()

# próg czułości czjników
GRD_TRESHOLD = 0.7

# pętla nieskończona
while True:
    # wyświetlenie stanu czujników
    print('GRD1:', grd1, 'GRD2:', grd2, 'GRD3:', grd3, 'GRD4:', grd4)

    # alternatywny sposób
    values = (grd1.value, grd2.value, grd3.value, grd4.value)
    #print('GRD1: %0.2f, GRD2: %0.2f, GRD3: %0.2f, GRD4: %0.2f' % values)
    
    # dioda świeci jeśli odczyt poniżej progu - robot widzi linię
    if grd1.value < GRD_TRESHOLD:
        led1.value = true
    else:
        led1.value = False
        
    # alternatywny zapis, kolejny czujnik
    led2.value = True if grd2.value < GRD_TRESHOLD else False
    
    time.sleep(1)
