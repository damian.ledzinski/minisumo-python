# Przykład demonstruje odczyt stanu czujników odległości i baterii

# Z czujników odległości jest odczytywane napięcie

# import bibliotek
from sumolib import *
import time

print('Przyklad 5')

# czujniki odległości
dist1, dist2, dist3, dist4 = dists_init()
# napięcie baterii
vBat = VBat()

# pętla nieskończona
while True:
    # wyświetlenie stanu czujników i napięcia baterii
    #print('DIST1:', dist1, 'DIST2:', dist2, 'DIST3:', dist3, 'V_BAT:', vBat)

    # alternatywny sposób
    values = (dist1.value, dist2.value, dist3.value, vBat.value)
    print('DIST1: %0.2f, DIST2: %0.2f, DIST3: %0.2f, V_BAT: %0.3f' % values)
    
    time.sleep(1)