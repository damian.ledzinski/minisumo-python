# Przykład demonstruje użycie przycisku i diod LED

# import bibliotek
from sumolib import *
import time

print('Przyklad 1')

# przycisk START1
start1 = Start1()
# przycisk BOOT1
boot1 = Boot1()
# diody
led1 = Led1()
led2 = Led2()

# zapalenie diody
led1.value = True

# pętla działająca do naciśnięcia przycisku BOOT1
while not boot1.value:
    # zapalenie diody w zależności od stanu przycisku START1
    led2.value = start1.value
    
# za pętlą zgaszenie diody LED1
led1.value = False

print('Koniec')
